package ru.t1.stepanischev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Category;
import ru.t1.stepanischev.tm.marker.DBCategory;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;
import ru.t1.stepanishchev.tm.api.service.dto.IProjectDTOService;
import ru.t1.stepanishchev.tm.api.service.dto.ISessionDTOService;
import ru.t1.stepanishchev.tm.api.service.dto.ITaskDTOService;
import ru.t1.stepanishchev.tm.api.service.dto.IUserDTOService;
import ru.t1.stepanishchev.tm.dto.model.UserDTO;
import ru.t1.stepanishchev.tm.service.ConnectionService;
import ru.t1.stepanishchev.tm.service.PropertyService;
import ru.t1.stepanishchev.tm.service.dto.ProjectDTOService;
import ru.t1.stepanishchev.tm.service.dto.SessionDTOService;
import ru.t1.stepanishchev.tm.service.dto.TaskDTOService;
import ru.t1.stepanishchev.tm.service.dto.UserDTOService;

import java.util.Arrays;
import java.util.List;

@Category(DBCategory.class)
public abstract class AbstractDTOServiceTest {

    @NotNull
    protected static IPropertyService propertyService = new PropertyService();

    @NotNull
    protected static IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    protected static ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @NotNull
    protected ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    protected IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    protected IUserDTOService userService = new UserDTOService(connectionService, propertyService, projectService, taskService, sessionService);

    @AfterClass
    public static void closeConnection() {
        connectionService.close();
    }

    @BeforeClass
    public static void initConnection() {
        connectionService = new ConnectionService(propertyService);
    }

    @After
    public void clearAllModel() {
        taskService.clear();
        projectService.clear();
        userService.clear();
    }

    @Before
    public void init() {
        sessionService = new SessionDTOService(connectionService);
        projectService = new ProjectDTOService(connectionService);
        taskService = new TaskDTOService(connectionService);
        userService = new UserDTOService(connectionService, propertyService, projectService, taskService, sessionService);
        @NotNull final UserDTO user1 = userService.create("user1", "user1", "user1@test.ru");
        user1.setId("userId-1");
        @NotNull final UserDTO user2 = userService.create("user2", "user2", "user2@test.ru");
        user2.setId("userId-2");
        @NotNull final List<UserDTO> list = Arrays.asList(user1, user2);
        userService.clear();
        userService.set(list);
    }

}