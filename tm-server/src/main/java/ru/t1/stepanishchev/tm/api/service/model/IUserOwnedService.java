package ru.t1.stepanishchev.tm.api.service.model;

import ru.t1.stepanishchev.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IAbstractService<M> {

}